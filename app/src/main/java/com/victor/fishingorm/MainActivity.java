package com.victor.fishingorm;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.victor.fishingorm.orm.DAO.HelperFactory;
import com.victor.fishingorm.orm.entities.Places;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener {

    private Button addButton;
    private Button getButton;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addButton = (Button) findViewById(R.id.test_button);
        getButton = (Button) findViewById(R.id.test_get);
        result = (TextView) findViewById(R.id.test_text);
        addButton.setOnClickListener(this);
        getButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.test_button:
                System.out.println("start");
                for (int i = 0; i < 20; i++) {
                    try {
                        Places newPlace = new Places("place" + i, 10.256 + i, 56.12578 + i);
                        System.out.println(newPlace.getId() + " " + newPlace.getTitle() + " " + newPlace.getLatitude());
                        HelperFactory.getHelper().getPlacesDAO().create(newPlace);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.test_get:
                System.out.println("finish");
                try {
                    List<Places> resultList = HelperFactory.getHelper().getPlacesDAO().getAllData();
                    Iterator iterator = resultList.iterator();
                    StringBuilder sb = new StringBuilder();
                    while (iterator.hasNext()) {
                        System.out.println("here");
                        Places p = (Places) iterator.next();
                        sb.append(p.getId()).append(p.getTitle()).append(p.getLatitude()).append(p.getLongtitude()).append("\n");
                    }
                    result.setText(sb.toString());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
