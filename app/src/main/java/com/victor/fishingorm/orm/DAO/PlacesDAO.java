package com.victor.fishingorm.orm.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.victor.fishingorm.orm.entities.Places;

import java.sql.SQLException;
import java.util.List;


public class PlacesDAO extends BaseDaoImpl<Places, Integer> {

    protected PlacesDAO(ConnectionSource connectionSource, Class<Places> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Places> getAllData() throws SQLException {
        return this.queryForAll();
    }

    public void insertIntoTable(Places p) throws SQLException {
        this.create(p);
    }

}
