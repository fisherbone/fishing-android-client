package com.victor.fishingorm.orm.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.victor.fishingorm.orm.entities.Fishes;

import java.sql.SQLException;
import java.util.List;


public class FishesDAO extends BaseDaoImpl<Fishes, Integer> {

    protected FishesDAO(ConnectionSource connectionSource, Class<Fishes> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Fishes> getAllData() throws SQLException {
        return this.queryForAll();
    }

    public void insertIntoTable(Fishes f) throws SQLException {
        this.create(f);
    }
}
