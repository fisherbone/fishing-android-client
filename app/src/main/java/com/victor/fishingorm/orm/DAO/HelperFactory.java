package com.victor.fishingorm.orm.DAO;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class HelperFactory {

    private static DatabaseHelper dataBaseHelper;

    public static DatabaseHelper getHelper() {
        return dataBaseHelper;
    }

    public static void setHelper(Context context) {
        dataBaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    public static void releaseHelper() {
        OpenHelperManager.releaseHelper();
        dataBaseHelper = null;
    }
}
