package com.victor.fishingorm.orm.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.victor.fishingorm.orm.entities.FishInPlace;
import com.victor.fishingorm.orm.entities.Fishes;
import com.victor.fishingorm.orm.entities.Places;

import java.sql.SQLException;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "fishing.db";

    private static final int DATABASE_VERSION = 1;

    private PlacesDAO placesDAO = null;
    private FishesDAO fishesDAO = null;
    private FishInPlaceDAO fishInPlaceDAO = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Places.class);
            TableUtils.createTable(connectionSource, FishInPlace.class);
            TableUtils.createTable(connectionSource, Fishes.class);
        } catch (SQLException ex) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Places.class, true);
            TableUtils.dropTable(connectionSource, FishInPlace.class, true);
            TableUtils.dropTable(connectionSource, Fishes.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    public PlacesDAO getPlacesDAO() throws SQLException{
        if(placesDAO == null){
            placesDAO = new PlacesDAO(getConnectionSource(), Places.class);
        }
        return placesDAO;
    }

    public FishInPlaceDAO getFishInPlaceDAO() throws SQLException{
        if(fishInPlaceDAO == null){
            fishInPlaceDAO = new FishInPlaceDAO(getConnectionSource(), FishInPlace.class);
        }
        return fishInPlaceDAO;
    }

    public FishesDAO getFishesDAO() throws SQLException{
        if(fishesDAO == null){
            fishesDAO = new FishesDAO(getConnectionSource(), Fishes.class);
        }
        return fishesDAO;
    }

    @Override
    public void close(){
        super.close();
        placesDAO = null;
        fishInPlaceDAO = null;
        fishesDAO = null;
    }
}
