package com.victor.fishingorm.orm.entities;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "fish_catalog")
public class Fishes {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String translit;

    @DatabaseField
    private String latName;

    @DatabaseField
    private double minWeight;

    @DatabaseField
    private double maxWeight;

    @DatabaseField
    private int minAllowedSize;

    @DatabaseField
    private String bestCatchStart;

    @DatabaseField
    private String bestCatchEnd;

    @DatabaseField
    private String bestCatchTime;

    @DatabaseField
    private String specConditions;

    @DatabaseField
    private int bestWaterTemperature;

    @DatabaseField
    private int spawnTemperature;

    @DatabaseField
    private String spawnStart;

    @DatabaseField
    private String spawnEnd;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] image;

    public Fishes() {
    }


    public Fishes(String name, String translit, String latName, double minWeight, double maxWeight, int minAllowedSize, String bestCatchStart, String bestCatchEnd, String bestCatchTime, String specConditions, int bestWaterTemperature, int spawnTemperature, String spawnStart, String spawnEnd, byte[] image) {
        this.name = name;
        this.translit = translit;
        this.latName = latName;
        this.minWeight = minWeight;
        this.maxWeight = maxWeight;
        this.minAllowedSize = minAllowedSize;
        this.bestCatchStart = bestCatchStart;
        this.bestCatchEnd = bestCatchEnd;
        this.bestCatchTime = bestCatchTime;
        this.specConditions = specConditions;
        this.bestWaterTemperature = bestWaterTemperature;
        this.spawnTemperature = spawnTemperature;
        this.spawnStart = spawnStart;
        this.spawnEnd = spawnEnd;
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTranslit(String translit) {
        this.translit = translit;
    }

    public void setLatName(String latName) {
        this.latName = latName;
    }

    public void setMinWeight(double minWeight) {
        this.minWeight = minWeight;
    }

    public void setMaxWeight(double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public void setMinAllowedSize(int minAllowedSize) {
        this.minAllowedSize = minAllowedSize;
    }

    public void setBestCatchStart(String bestCatchStart) {
        this.bestCatchStart = bestCatchStart;
    }

    public void setBestCatchEnd(String bestCatchEnd) {
        this.bestCatchEnd = bestCatchEnd;
    }

    public void setBestCatchTime(String bestCatchTime) {
        this.bestCatchTime = bestCatchTime;
    }

    public void setSpecConditions(String specConditions) {
        this.specConditions = specConditions;
    }

    public void setBestWaterTemperature(int bestWaterTemperature) {
        this.bestWaterTemperature = bestWaterTemperature;
    }

    public void setSpawnTemperature(int spawnTemperature) {
        this.spawnTemperature = spawnTemperature;
    }

    public void setSpawnStart(String spawnStart) {
        this.spawnStart = spawnStart;
    }

    public void setSpawnEnd(String spawnEnd) {
        this.spawnEnd = spawnEnd;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getTranslit() {
        return translit;
    }

    public String getLatName() {
        return latName;
    }

    public double getMinWeight() {
        return minWeight;
    }

    public double getMaxWeight() {
        return maxWeight;
    }

    public int getMinAllowedSize() {
        return minAllowedSize;
    }

    public String getBestCatchStart() {
        return bestCatchStart;
    }

    public String getBestCatchEnd() {
        return bestCatchEnd;
    }

    public String getBestCatchTime() {
        return bestCatchTime;
    }

    public String getSpecConditions() {
        return specConditions;
    }

    public int getBestWaterTemperature() {
        return bestWaterTemperature;
    }

    public int getSpawnTemperature() {
        return spawnTemperature;
    }

    public String getSpawnStart() {
        return spawnStart;
    }

    public String getSpawnEnd() {
        return spawnEnd;
    }

    public byte[] getImage() {
        return image;
    }

    public int getId() {
        return id;
    }
}
