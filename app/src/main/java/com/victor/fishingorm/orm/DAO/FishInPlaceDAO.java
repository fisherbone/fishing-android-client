package com.victor.fishingorm.orm.DAO;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.victor.fishingorm.orm.entities.FishInPlace;

import java.sql.SQLException;
import java.util.List;


public class FishInPlaceDAO extends BaseDaoImpl<FishInPlace, Integer> {

    protected FishInPlaceDAO(ConnectionSource connectionSource, Class<FishInPlace> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<FishInPlace> getAllData() throws SQLException {
        return this.queryForAll();
    }

    public void insertIntoTable(FishInPlace fip) throws SQLException {
        this.create(fip);
    }
}
