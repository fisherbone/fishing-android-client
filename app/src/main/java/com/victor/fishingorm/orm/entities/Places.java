package com.victor.fishingorm.orm.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable (tableName = "places")
public class Places {

    @DatabaseField (generatedId = true)
    private int id;

    @DatabaseField
    private String title;

    @DatabaseField
    private double latitude;

    @DatabaseField
    private double longtitude;

    public Places(){
    }

    public Places(String title, double latitude, double longtitude){
        this.title = title;
        this.latitude = latitude;
        this.longtitude = longtitude;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getTitle() {

        return title;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public int getId() {
        return id;
    }
}
