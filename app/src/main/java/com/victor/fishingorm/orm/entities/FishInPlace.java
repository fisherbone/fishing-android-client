package com.victor.fishingorm.orm.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "fish_in_place")
public class FishInPlace {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    private int fishId;

    @DatabaseField(canBeNull = false)
    private int placeId;

    public FishInPlace() {
    }

    public FishInPlace(int fishId, int placeId) {
        this.fishId = fishId;
        this.placeId = placeId;
    }

    public void setFishId(int fishId) {
        this.fishId = fishId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getFishId() {

        return fishId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public int getId() {
        return id;
    }
}
