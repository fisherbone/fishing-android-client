package com.victor.fishingorm;

import android.app.Application;

import com.victor.fishingorm.orm.DAO.HelperFactory;


public class FishingApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        HelperFactory.releaseHelper();
    }
}
